LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libpvlogger
LOCAL_DESCRIPTION := PV Logger library

LOCAL_LDFLAGS := --static

LOCAL_SRC_FILES := logger.c

LOCAL_INSTALL_HEADERS := logger.h

include $(BUILD_STATIC_LIBRARY)
